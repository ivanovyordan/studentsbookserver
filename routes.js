var fs = require('fs');

/* Predefined routes */
server.get('/', function(request, response){
	response.send(200, 'HELP');
});

server.post('/login', function(request, response, callback){
	var controller = require(__dirname + '/controllers/UserController');
	controller.login(request, response);
	return callback();
});


/* Standart routing */
try {
	files = fs.readdirSync(__dirname + '/controllers');
	files.forEach(function(file){
		var controllerName = file.split('Controller')[1].toLowerCase();
		
		server.get(controllerName + 's', function(){
			var controller = require(__dirname + '/controllers/' + file);
			if(controller.list !== undefined) {
				controller.list(request, response);
			} else {
				response.send(404);
			}
		});
		
		server.get(controllerName + 's/count', function(){
			var controller = require(__dirname + '/controllers/' + file);
			if(controller.count !== undefined) {
				controller.count(request, response);
			} else {
				response.send(404);
			}
		});
		
		server.get(controllerName + '/:id', function(){
			var controller = require(__dirname + '/controllers/' + file);
			if(controller.view !== undefined) {
				controller.view(request, response);
			} else {
				response.send(404);
			}
		});
		
		server.post(controllerName, function(){
			var controller = require(__dirname + '/controllers/' + file);
			if(controller.create !== undefined) {
				controller.create(request, response);
			} else {
				response.send(404);
			}
		});
		
		server.put(controllerName, function(){
			var controller = require(__dirname + '/controllers/' + file);
			if(controller.update !== undefined) {
				controller.update(request, response);
			} else {
				response.send(404);
			}
		});
		
		server.del(controllerName, function(){
			var controller = require(__dirname + '/controllers/' + file);
			if(controller.remove !== undefined) {
				controller.remove(request, response);
			} else {
				response.send(404);
			}
		});
	});
} catch(error) {
	console.log(error);
	process.exit();
}