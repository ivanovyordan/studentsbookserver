module.exports = function(sequelize)
{
	var Sequelize = sequelize.module,
		client = sequelize.client;
	
	var Trainer = client.define('trainer', {
		// Field validators
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		name: {type: Sequelize.STRING, validate: {notEmpty: true, len: [1, 255]}},
		username: {type: Sequelize.STRING, unique: true, validate: {notEmpty: true, len: [1, 255]}},
		email: {type: Sequelize.STRING, unique: true, validate: {notEmpty: true, isEmail: true, len: [1, 255]}},
		password: {type: Sequelize.STRING, validate: {notEmpty: true, len: [1, 255]}},
		group_id: {type: Sequelize.INTEGER, validate: {equals: 2}}
	}, {
		classMethods: {
		},
		instanceMethods: {
			mapAttributes: sequelize.map
		}
	});
	
	return Trainer;
};