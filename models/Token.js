var utils = require(__dirname + '/../utils');

module.exports = function(sequelize)
{
	var Sequelize = sequelize.module,
		client = sequelize.client;
	
	var Token = client.define('token', {
		// Field validators
		id: {type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true, allowNull: false},
		token: {type: Sequelize.STRING, defaultValue: utils.generateToken(48), validate: {notNull: true, isAlphanumeric: true, len:[2, 255]}},
		user_id: {type: Sequelize.INTEGER, validate: {notNull: true, isNumeric: true}},
		group_id: {type: Sequelize.INTEGER, validate: {idn: [1,2,3]}},
		registred: {type: Sequelize.INTEGER, defaultValue: utils.now(), validate: {notNull: true, isData: true}}
	}, {
		classMethods: {
		},
		instanceMethods: {
			mapAttributes: sequelize.map
		}
	});
	Token.belongsTo(sequelize.models.User);
	
	return Token;
};