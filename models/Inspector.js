module.exports = function(sequelize)
{
	var Sequelize = sequelize.module,
		client = sequelize.client;
	
	var Inspector = client.define('inspector', {
		// Field validators
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		name: {type: Sequelize.STRING, validate: {notEmpty: true, len: [1, 255]}},
		username: {type: Sequelize.STRING, unique: true, validate: {notEmpty: true, len: [1, 255]}},
		email: {type: Sequelize.STRING, unique: true, validate: {notEmpty: true, isEmail: true, len: [1, 255]}},
		password: {type: Sequelize.STRING, validate: {notEmpty: true, len: [1, 255]}},
		group_id: {type: Sequelize.INTEGER, validate: {equals: 1}}
	}, {
		classMethods: {
		},
		instanceMethods: {
			mapAttributes: sequelize.map
		}
	});
	
	return Inspector;
};