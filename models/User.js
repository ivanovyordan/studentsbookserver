var ROLE_INSPECTOR = 1,
	ROLE_TRAINER = 2,
	ROLE_STUDENT = 3;

module.exports = function(sequelize)
{
	var Sequelize = sequelize.module,
		client = sequelize.client;
	
	var User = client.define('user', {
		// Field validators
		id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		name: {type: Sequelize.STRING, validate: {notEmpty: true, len: [1, 255]}},
		username: {type: Sequelize.STRING, unique: true, validate: {notEmpty: true, len: [1, 255]}},
		email: {type: Sequelize.STRING, unique: true, validate: {notEmpty: true, isEmail: true, len: [1, 255]}},
		password: {type: Sequelize.STRING, validate: {notEmpty: true, len: [1, 255]}},
		group_id: {type: Sequelize.INTEGER, validate: {isIn: [1,2,3]}},
		course_id: {type: Sequelize.INTEGER, validate: {isNumeric: true}},
		certified: {type: Sequelize.BOOLEAN}
	}, {
		classMethods: {
			login: function(username, password, callback)
			{
				password = require('crypto').createHash('md5').update(password).digest('hex');
				this.find({
					where: ['(username = ? OR email = ?) AND password = ?', username, username, password]
				}).success(function(user){
					if(user === null) {
						callback(null);
						return;
					}
					
					var userType = null;
					switch(user.group_id) {
						case ROLE_INSPECTOR:
							userType = sequelize.models.Inspector;
							break;
						case ROLE_TRAINER:
							userType = sequelize.models.Trainer;
							break;
						case ROLE_STUDENT:
							userType = sequelize.models.Student;
							break;
						default:
							callback(null);
							return;
					}
					
					userType.find(user.id).success(function(u){
						callback(u);
					});
				}).error(function(e){
					console.log(e);
					callback(null);
				});
			}
		},
		instanceMethods: {
			mapAttributes: sequelize.map
		}
	});
	
	return User;
};