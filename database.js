var config = require(__dirname + '/config'),
database = {
	options: config.db
};

var Sequelize = require('sequelize');
database.module = Sequelize;
database.client = new Sequelize(config.db.database, config.db.username, config.db.password,{
	host: config.db.host,
	port: config.db.port,
	dialect: config.db.dialect,
	
	define: {
		underscored: true,
		freezeTableName: true,
		timestamps: false
	}
});


/**
* @type {Object}
* Map all attributes of the registry
* (Instance method useful to every sequelize Table)
* @this {SequelizeRegistry}
* @return {Object} All attributes in a Object.
*/
database.map = function() {
	var obj = {},
		ctx = this;
	
	ctx.attributes.forEach(function(attr) {
		obj[attr] = ctx[attr];
	});
	
	return obj;
};

/* Load Models */
var fs = require('fs');
database.models = {};
try {
	files = fs.readdirSync(__dirname + '/models');
	files.forEach(function(file){
		var model = require(__dirname + '/models/' + file)(database);
		model.sync();
		database.models[file.split('.')[0]] = model;
	});
} catch(error) {
	console.log(error);
	process.exit();
}

module.exports = database;