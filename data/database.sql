SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 194 (class 3079 OID 11679)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 194
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 161 (class 1259 OID 16385)
-- Dependencies: 6
-- Name: course; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE course (
    id integer NOT NULL,
    specialty_id integer NOT NULL,
    education_form_id integer NOT NULL,
    beggining_year smallint NOT NULL
);


ALTER TABLE public.course OWNER TO postgres;

--
-- TOC entry 162 (class 1259 OID 16388)
-- Dependencies: 6 161
-- Name: course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_id_seq OWNER TO postgres;

--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 162
-- Name: course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE course_id_seq OWNED BY course.id;


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 162
-- Name: course_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('course_id_seq', 1, false);


--
-- TOC entry 163 (class 1259 OID 16390)
-- Dependencies: 6
-- Name: course_subject; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE course_subject (
    id integer NOT NULL,
    course_id integer NOT NULL,
    subject_id integer NOT NULL,
    year smallint NOT NULL,
    semester smallint NOT NULL
);


ALTER TABLE public.course_subject OWNER TO postgres;

--
-- TOC entry 164 (class 1259 OID 16393)
-- Dependencies: 6 163
-- Name: course_subject_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE course_subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_subject_id_seq OWNER TO postgres;

--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 164
-- Name: course_subject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE course_subject_id_seq OWNED BY course_subject.id;


--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 164
-- Name: course_subject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('course_subject_id_seq', 1, false);


--
-- TOC entry 165 (class 1259 OID 16395)
-- Dependencies: 6
-- Name: education_form; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE education_form (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.education_form OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 16398)
-- Dependencies: 165 6
-- Name: education_form_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE education_form_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.education_form_id_seq OWNER TO postgres;

--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 166
-- Name: education_form_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE education_form_id_seq OWNED BY education_form.id;


--
-- TOC entry 2095 (class 0 OID 0)
-- Dependencies: 166
-- Name: education_form_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('education_form_id_seq', 1, false);


--
-- TOC entry 167 (class 1259 OID 16400)
-- Dependencies: 6
-- Name: user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "user" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255),
    username character varying(255) NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 168 (class 1259 OID 16406)
-- Dependencies: 167 6
-- Name: inspector; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE inspector (
)
INHERITS ("user");


ALTER TABLE public.inspector OWNER TO postgres;

--
-- TOC entry 169 (class 1259 OID 16412)
-- Dependencies: 6
-- Name: log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE log (
    id integer NOT NULL,
    uri character varying(255) NOT NULL,
    method character varying(6) NOT NULL,
    params text,
    token_id integer,
    ip_address character varying(15) NOT NULL,
    "time" integer
);


ALTER TABLE public.log OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 16418)
-- Dependencies: 169 6
-- Name: log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_id_seq OWNER TO postgres;

--
-- TOC entry 2096 (class 0 OID 0)
-- Dependencies: 170
-- Name: log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_id_seq OWNED BY log.id;


--
-- TOC entry 2097 (class 0 OID 0)
-- Dependencies: 170
-- Name: log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('log_id_seq', 1, false);


--
-- TOC entry 171 (class 1259 OID 16420)
-- Dependencies: 6
-- Name: mark; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE mark (
    id integer NOT NULL,
    student_id integer NOT NULL,
    course_subject_id integer NOT NULL,
    session_id integer NOT NULL,
    is_new boolean NOT NULL,
    value smallint NOT NULL,
    comment character varying(255) NOT NULL,
    type_id smallint NOT NULL
);


ALTER TABLE public.mark OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16423)
-- Dependencies: 6 171
-- Name: mark_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE mark_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mark_id_seq OWNER TO postgres;

--
-- TOC entry 2098 (class 0 OID 0)
-- Dependencies: 172
-- Name: mark_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE mark_id_seq OWNED BY mark.id;


--
-- TOC entry 2099 (class 0 OID 0)
-- Dependencies: 172
-- Name: mark_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('mark_id_seq', 1, false);


--
-- TOC entry 173 (class 1259 OID 16425)
-- Dependencies: 6
-- Name: mark_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE mark_type (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.mark_type OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 16428)
-- Dependencies: 173 6
-- Name: mark_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE mark_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mark_type_id_seq OWNER TO postgres;

--
-- TOC entry 2100 (class 0 OID 0)
-- Dependencies: 174
-- Name: mark_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE mark_type_id_seq OWNED BY mark_type.id;


--
-- TOC entry 2101 (class 0 OID 0)
-- Dependencies: 174
-- Name: mark_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('mark_type_id_seq', 1, false);


--
-- TOC entry 175 (class 1259 OID 16430)
-- Dependencies: 6
-- Name: recess_tools_classes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE recess_tools_classes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recess_tools_classes_id_seq OWNER TO postgres;

--
-- TOC entry 2102 (class 0 OID 0)
-- Dependencies: 175
-- Name: recess_tools_classes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('recess_tools_classes_id_seq', 1, false);


--
-- TOC entry 176 (class 1259 OID 16432)
-- Dependencies: 1996 6
-- Name: recess_tools_classes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE recess_tools_classes (
    id integer DEFAULT nextval('recess_tools_classes_id_seq'::regclass) NOT NULL,
    name character varying(255),
    "parentId" integer,
    "packageId" integer,
    "docComment" text,
    file text
);


ALTER TABLE public.recess_tools_classes OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 16439)
-- Dependencies: 6
-- Name: recess_tools_packages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE recess_tools_packages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recess_tools_packages_id_seq OWNER TO postgres;

--
-- TOC entry 2103 (class 0 OID 0)
-- Dependencies: 177
-- Name: recess_tools_packages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('recess_tools_packages_id_seq', 1, false);


--
-- TOC entry 178 (class 1259 OID 16441)
-- Dependencies: 1997 6
-- Name: recess_tools_packages; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE recess_tools_packages (
    id integer DEFAULT nextval('recess_tools_packages_id_seq'::regclass) NOT NULL,
    name character varying(255),
    "parentId" integer
);


ALTER TABLE public.recess_tools_packages OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 16445)
-- Dependencies: 6
-- Name: session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE session (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.session OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 16448)
-- Dependencies: 179 6
-- Name: session_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.session_id_seq OWNER TO postgres;

--
-- TOC entry 2104 (class 0 OID 0)
-- Dependencies: 180
-- Name: session_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE session_id_seq OWNED BY session.id;


--
-- TOC entry 2105 (class 0 OID 0)
-- Dependencies: 180
-- Name: session_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('session_id_seq', 1, false);


--
-- TOC entry 181 (class 1259 OID 16450)
-- Dependencies: 6
-- Name: specialty; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE specialty (
    id integer NOT NULL,
    name bit varying(255) NOT NULL
);


ALTER TABLE public.specialty OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16453)
-- Dependencies: 181 6
-- Name: specialty_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE specialty_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.specialty_id_seq OWNER TO postgres;

--
-- TOC entry 2106 (class 0 OID 0)
-- Dependencies: 182
-- Name: specialty_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE specialty_id_seq OWNED BY specialty.id;


--
-- TOC entry 2107 (class 0 OID 0)
-- Dependencies: 182
-- Name: specialty_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('specialty_id_seq', 1, false);


--
-- TOC entry 183 (class 1259 OID 16455)
-- Dependencies: 6 167
-- Name: student; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE student (
    fac_number character varying(10) NOT NULL
)
INHERITS ("user");


ALTER TABLE public.student OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 16461)
-- Dependencies: 6
-- Name: subject; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE subject (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.subject OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16464)
-- Dependencies: 184 6
-- Name: subject_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subject_id_seq OWNER TO postgres;

--
-- TOC entry 2108 (class 0 OID 0)
-- Dependencies: 185
-- Name: subject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE subject_id_seq OWNED BY subject.id;


--
-- TOC entry 2109 (class 0 OID 0)
-- Dependencies: 185
-- Name: subject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('subject_id_seq', 1, false);


--
-- TOC entry 186 (class 1259 OID 16466)
-- Dependencies: 6
-- Name: token; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE token (
    id integer NOT NULL,
    token character varying(255),
    user_id integer NOT NULL,
    registred integer NOT NULL
);


ALTER TABLE public.token OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 16469)
-- Dependencies: 186 6
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.token_id_seq OWNER TO postgres;

--
-- TOC entry 2110 (class 0 OID 0)
-- Dependencies: 187
-- Name: token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE token_id_seq OWNED BY token.id;


--
-- TOC entry 2111 (class 0 OID 0)
-- Dependencies: 187
-- Name: token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('token_id_seq', 17, true);


--
-- TOC entry 188 (class 1259 OID 16471)
-- Dependencies: 167 6
-- Name: trainer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trainer (
)
INHERITS ("user");


ALTER TABLE public.trainer OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 16477)
-- Dependencies: 6
-- Name: trainer_course_subject; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trainer_course_subject (
    id integer NOT NULL,
    course_subject_id integer NOT NULL,
    trainer_id integer NOT NULL
);


ALTER TABLE public.trainer_course_subject OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 16480)
-- Dependencies: 189 6
-- Name: trainer_course_subject_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE trainer_course_subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trainer_course_subject_id_seq OWNER TO postgres;

--
-- TOC entry 2112 (class 0 OID 0)
-- Dependencies: 190
-- Name: trainer_course_subject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE trainer_course_subject_id_seq OWNED BY trainer_course_subject.id;


--
-- TOC entry 2113 (class 0 OID 0)
-- Dependencies: 190
-- Name: trainer_course_subject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('trainer_course_subject_id_seq', 1, false);


--
-- TOC entry 191 (class 1259 OID 16482)
-- Dependencies: 6
-- Name: user_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_group (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_group OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 16485)
-- Dependencies: 6 191
-- Name: user_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_group_id_seq OWNER TO postgres;

--
-- TOC entry 2114 (class 0 OID 0)
-- Dependencies: 192
-- Name: user_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_group_id_seq OWNED BY user_group.id;


--
-- TOC entry 2115 (class 0 OID 0)
-- Dependencies: 192
-- Name: user_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_group_id_seq', 6, true);


--
-- TOC entry 193 (class 1259 OID 16487)
-- Dependencies: 6 167
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- TOC entry 2116 (class 0 OID 0)
-- Dependencies: 193
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- TOC entry 2117 (class 0 OID 0)
-- Dependencies: 193
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_id_seq', 2, true);


--
-- TOC entry 1988 (class 2604 OID 16489)
-- Dependencies: 162 161
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY course ALTER COLUMN id SET DEFAULT nextval('course_id_seq'::regclass);


--
-- TOC entry 1989 (class 2604 OID 16490)
-- Dependencies: 164 163
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY course_subject ALTER COLUMN id SET DEFAULT nextval('course_subject_id_seq'::regclass);


--
-- TOC entry 1990 (class 2604 OID 16491)
-- Dependencies: 166 165
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY education_form ALTER COLUMN id SET DEFAULT nextval('education_form_id_seq'::regclass);


--
-- TOC entry 1992 (class 2604 OID 16492)
-- Dependencies: 168 168 193
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inspector ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- TOC entry 1993 (class 2604 OID 16493)
-- Dependencies: 170 169
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log ALTER COLUMN id SET DEFAULT nextval('log_id_seq'::regclass);


--
-- TOC entry 1994 (class 2604 OID 16494)
-- Dependencies: 172 171
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mark ALTER COLUMN id SET DEFAULT nextval('mark_id_seq'::regclass);


--
-- TOC entry 1995 (class 2604 OID 16495)
-- Dependencies: 174 173
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mark_type ALTER COLUMN id SET DEFAULT nextval('mark_type_id_seq'::regclass);


--
-- TOC entry 1998 (class 2604 OID 16496)
-- Dependencies: 180 179
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY session ALTER COLUMN id SET DEFAULT nextval('session_id_seq'::regclass);


--
-- TOC entry 1999 (class 2604 OID 16497)
-- Dependencies: 182 181
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY specialty ALTER COLUMN id SET DEFAULT nextval('specialty_id_seq'::regclass);


--
-- TOC entry 2000 (class 2604 OID 16498)
-- Dependencies: 183 183 193
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- TOC entry 2001 (class 2604 OID 16499)
-- Dependencies: 185 184
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subject ALTER COLUMN id SET DEFAULT nextval('subject_id_seq'::regclass);


--
-- TOC entry 2002 (class 2604 OID 16500)
-- Dependencies: 187 186
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY token ALTER COLUMN id SET DEFAULT nextval('token_id_seq'::regclass);


--
-- TOC entry 2003 (class 2604 OID 16501)
-- Dependencies: 188 193 188
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY trainer ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- TOC entry 2004 (class 2604 OID 16502)
-- Dependencies: 190 189
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY trainer_course_subject ALTER COLUMN id SET DEFAULT nextval('trainer_course_subject_id_seq'::regclass);


--
-- TOC entry 1991 (class 2604 OID 16503)
-- Dependencies: 193 167
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- TOC entry 2005 (class 2604 OID 16504)
-- Dependencies: 192 191
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_group ALTER COLUMN id SET DEFAULT nextval('user_group_id_seq'::regclass);

--
-- TOC entry 2007 (class 2606 OID 16506)
-- Dependencies: 161 161
-- Name: course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY course
    ADD CONSTRAINT course_pkey PRIMARY KEY (id);


--
-- TOC entry 2009 (class 2606 OID 16508)
-- Dependencies: 161 161 161 161
-- Name: course_specialty_id_education_form_id_beggining_year_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY course
    ADD CONSTRAINT course_specialty_id_education_form_id_beggining_year_key UNIQUE (specialty_id, education_form_id, beggining_year);


--
-- TOC entry 2011 (class 2606 OID 16510)
-- Dependencies: 163 163 163 163 163
-- Name: course_subject_course_id_subject_id_year_semester_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY course_subject
    ADD CONSTRAINT course_subject_course_id_subject_id_year_semester_key UNIQUE (course_id, subject_id, year, semester);


--
-- TOC entry 2013 (class 2606 OID 16512)
-- Dependencies: 163 163
-- Name: course_subject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY course_subject
    ADD CONSTRAINT course_subject_pkey PRIMARY KEY (id);


--
-- TOC entry 2015 (class 2606 OID 16514)
-- Dependencies: 165 165
-- Name: education_form_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY education_form
    ADD CONSTRAINT education_form_name_key UNIQUE (name);


--
-- TOC entry 2017 (class 2606 OID 16516)
-- Dependencies: 165 165
-- Name: education_form_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY education_form
    ADD CONSTRAINT education_form_pkey PRIMARY KEY (id);


--
-- TOC entry 2025 (class 2606 OID 16518)
-- Dependencies: 168 168
-- Name: inspector_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY inspector
    ADD CONSTRAINT inspector_pkey PRIMARY KEY (id);


--
-- TOC entry 2027 (class 2606 OID 16520)
-- Dependencies: 169 169
-- Name: log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY log
    ADD CONSTRAINT log_pkey PRIMARY KEY (id);


--
-- TOC entry 2029 (class 2606 OID 16522)
-- Dependencies: 171 171
-- Name: mark_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mark
    ADD CONSTRAINT mark_pkey PRIMARY KEY (id);


--
-- TOC entry 2031 (class 2606 OID 16524)
-- Dependencies: 173 173
-- Name: mark_type_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mark_type
    ADD CONSTRAINT mark_type_name_key UNIQUE (name);


--
-- TOC entry 2033 (class 2606 OID 16526)
-- Dependencies: 173 173
-- Name: mark_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mark_type
    ADD CONSTRAINT mark_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2035 (class 2606 OID 16528)
-- Dependencies: 176 176
-- Name: recess_tools_classes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY recess_tools_classes
    ADD CONSTRAINT recess_tools_classes_pkey PRIMARY KEY (id);


--
-- TOC entry 2037 (class 2606 OID 16530)
-- Dependencies: 178 178
-- Name: recess_tools_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY recess_tools_packages
    ADD CONSTRAINT recess_tools_packages_pkey PRIMARY KEY (id);


--
-- TOC entry 2039 (class 2606 OID 16532)
-- Dependencies: 179 179
-- Name: session_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY session
    ADD CONSTRAINT session_name_key UNIQUE (name);


--
-- TOC entry 2041 (class 2606 OID 16534)
-- Dependencies: 179 179
-- Name: session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY session
    ADD CONSTRAINT session_pkey PRIMARY KEY (id);


--
-- TOC entry 2043 (class 2606 OID 16536)
-- Dependencies: 181 181
-- Name: specialty_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY specialty
    ADD CONSTRAINT specialty_name_key UNIQUE (name);


--
-- TOC entry 2045 (class 2606 OID 16538)
-- Dependencies: 181 181
-- Name: specialty_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY specialty
    ADD CONSTRAINT specialty_pkey PRIMARY KEY (id);


--
-- TOC entry 2047 (class 2606 OID 16540)
-- Dependencies: 183 183
-- Name: student_fac_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student
    ADD CONSTRAINT student_fac_number_key UNIQUE (fac_number);


--
-- TOC entry 2049 (class 2606 OID 16542)
-- Dependencies: 183 183
-- Name: student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student
    ADD CONSTRAINT student_pkey PRIMARY KEY (id);


--
-- TOC entry 2051 (class 2606 OID 16544)
-- Dependencies: 184 184
-- Name: subject_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_name_key UNIQUE (name);


--
-- TOC entry 2053 (class 2606 OID 16546)
-- Dependencies: 184 184
-- Name: subject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (id);


--
-- TOC entry 2055 (class 2606 OID 16548)
-- Dependencies: 186 186
-- Name: token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY token
    ADD CONSTRAINT token_pkey PRIMARY KEY (id);


--
-- TOC entry 2059 (class 2606 OID 16550)
-- Dependencies: 189 189 189
-- Name: trainer_course_subject_course_subject_id_trainer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trainer_course_subject
    ADD CONSTRAINT trainer_course_subject_course_subject_id_trainer_id_key UNIQUE (course_subject_id, trainer_id);


--
-- TOC entry 2061 (class 2606 OID 16552)
-- Dependencies: 189 189
-- Name: trainer_course_subject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trainer_course_subject
    ADD CONSTRAINT trainer_course_subject_pkey PRIMARY KEY (id);


--
-- TOC entry 2057 (class 2606 OID 16554)
-- Dependencies: 188 188
-- Name: trainer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trainer
    ADD CONSTRAINT trainer_pkey PRIMARY KEY (id);


--
-- TOC entry 2019 (class 2606 OID 16556)
-- Dependencies: 167 167
-- Name: user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- TOC entry 2063 (class 2606 OID 16558)
-- Dependencies: 191 191
-- Name: user_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_group
    ADD CONSTRAINT user_group_name_key UNIQUE (name);


--
-- TOC entry 2065 (class 2606 OID 16560)
-- Dependencies: 191 191
-- Name: user_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_group
    ADD CONSTRAINT user_group_pkey PRIMARY KEY (id);


--
-- TOC entry 2021 (class 2606 OID 16562)
-- Dependencies: 167 167
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2023 (class 2606 OID 16564)
-- Dependencies: 167 167
-- Name: user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_username_key UNIQUE (username);


INSERT INTO trainer(id, name, email, password, username, group_id)
VALUES (1, 'проф. д-р Станимир Стоянов', 'stani@uni-plovdiv.bg', '5f4dcc3b5aa765d61d8327deb882cf99', 'trainer', 2);

INSERT INTO user_group(id, name) VALUES
(1, 'Инспектор'),
(2, 'Преподавател'),
(3, 'Студент');


REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;