String.prototype.firstUpper = function()
{
	return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
};

exports.generateToken = function(length)
{
	try {
		var token = require('crypto').randomBytes(256).toString('hex').substr(-length);
		return token;
	} catch(error) {
		console.log(error);
		return 0;
	}
};

exports.now = function()
{
	var date = new Date();
	return String(Math.round(date.getTime() / 1000) + date.getTimezoneOffset() * 60);
};