var config = {};

config.server = {};
config.server.port = 8000;

config.db = {};
config.db.host = 'localhost';
config.db.port = 5432;
config.db.dialect = 'postgres';
config.db.database = 'students_book';
config.db.username = 'postgres';
config.db.password = 'password';

module.exports =config;