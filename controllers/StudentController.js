exports.count = function(request, response)
{
	if(request.headers['X-USER-TOKEN'] === undefined) {
		console.log('wrong header');
		response.send(401);
		return;
	}
	
	var where = {};
	if(request.body.course !== undefined) {
		where.course_id = request.body.course;
	}
	
	if(request.body.certified !== undefined) {
		where.certified = request.body.certified;
	}
	
	// TODO: Success stats
	sequelize.models.User.count(where).success(function(result){
		response.send(200, result);
	});
};