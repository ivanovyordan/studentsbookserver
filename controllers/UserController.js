exports.list = function (request, response)
{
	console.log('list');
	response.send(200);
};
	
exports.view = function(request, response)
{
	console.log('view');
	response.send(200);
};

exports.login = function(request, response)
{
	console.log(request.header('dancho'));
	if(request.params.username === undefined || request.params.password === undefined) {
		response.send(412, 'Username or Password Not Set');
		return;
	}
	
	sequelize.models.User.login(request.params.username, request.params.password, function(user){
		if(user === null) {
			response.send(401, 'Invalid username or password');
		} else {
			user = user.mapAttributes();
			
			sequelize.models.Token.create({user_id: user.id, group_id: user.group_id}).success(function(token){
				delete user.id;
				delete user.password;
				user.token = token.token;
				response.send(200, user);
			});
		}
	});
};