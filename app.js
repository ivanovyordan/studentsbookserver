var restify = require('restify'),
	fs = require('fs'),
	config = require('./config');

require('./utils');

// Create Application Server
var server = module.exports = restify.createServer({
	name: 'Book API'
});

/* Server cinfiguration */
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

GLOBAL.server = server;

// Set Route Init Path
var routes = require(__dirname + '/routes');

// Set Database Init Path
GLOBAL.sequelize = require(__dirname + '/database');

/**
* Retrieve Command Line Arguments
* [0] process : String 'node'
* [1] app : void
* [2] port : Number From config
*/
var args = process.argv,
port = args[2] ? args[2] : config.server.port;

server.listen(port, function(){
	console.log('%s listening at %s', server.name, server.url);
});